package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

public class CalculatorTest
{
    @ParameterizedTest(name = "Asserting length of {0} is < 5")
    @ValueSource(strings = {"ahoj","123","12345"})
    public void FooTest(String input)
    {
        Assertions.assertTrue(input.length() < 5);
    }

    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource(value = {"9:5:14","0:-1:-1"}, delimiter = ':')
    public void AdditionTest(int a, int b, int expected)
    {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int result = calculator.add(a, b);

        //ASSERT
        Assertions.assertEquals(expected, result);
    }

    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvFileSource(resources = "/file.csv", delimiter = ':', numLinesToSkip = 2)
    public void AdditionTestFileSource(int a, int b, int expected)
    {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int result = calculator.add(a, b);

        //ASSERT
        Assertions.assertEquals(expected, result);
    }

    private static Stream<Arguments> provideArgumentsForAdditionTest()
    {
        return Stream.of(Arguments.of(1,2,3),Arguments.of(2,2,4),Arguments.of(0,0,1));
    }

    @ParameterizedTest(name = "{0} + {1} = {2}")
    @MethodSource("provideArgumentsForAdditionTest")
    public void AdditionTestMethodSource(int a, int b, int expected)
    {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int result = calculator.add(a, b);

        //ASSERT
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void Addition_9Plus5_14()
    {
        //ARRANGE
        Calculator calculator = new Calculator();
        int a = 9;
        int b = 5;
        int expected = 14;

        //ACT
        int result = calculator.add(a, b);

        //ASSERT
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void Subtraction_9Minus5_4()
    {
        //ARRANGE
        Calculator calculator = new Calculator();
        int a = 9;
        int b = 5;
        int expected = 4;

        //ACT
        int result = calculator.subtract(a, b);

        //ASSERT
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void Multiplication_9Multiply5_45()
    {
        //ARRANGE
        Calculator calculator = new Calculator();
        int a = 9;
        int b = 5;
        int expected = 45;

        //ACT
        int result = calculator.multiply(a, b);

        //ASSERT
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void Devision_10DevidedBy5_2()
    {
        //ARRANGE
        Calculator calculator = new Calculator();
        int a = 10;
        int b = 5;
        int expected = 2;

        //ACT
        int result = calculator.divide(a, b);

        //ASSERT
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void devision_10devidedBy0_ThrowsExeption()
    {
        //ARRANGE
        Calculator calculator = new Calculator();
        int a = 10;
        int b = 0;

        //ACT
        //int result = calculator.multiply(a, b);

        //ASSERT
        Assertions.assertThrows(Exception.class, () -> {calculator.divide(a, b);});
    }
}

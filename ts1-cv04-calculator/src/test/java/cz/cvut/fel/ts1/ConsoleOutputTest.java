package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ConsoleOutputTest
{
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStream()
    {
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void ConsoleOutputTest()
    {
        //Arrange
        ConsoleHandler consoleHandler = new ConsoleHandler();

        //Act
        consoleHandler.DoStuff();

        //Assertion
        Assertions.assertEquals("Within", outContent.toString());
    }

    @AfterEach
    public void TearDownStream()
    {
        System.setOut(originalOut);
    }
}

package archive;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import shop.DiscountedItem;
import shop.Item;
import shop.Order;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PurchasesArchiveTest
{
    //testy metod printItemPurchaseStatistics, getHowManyTimesHasBeenItemSold a putOrderToPurchasesArchive
    //println (stream)
    //mock orderArchive
    //mock ItemPurchaseArchiveEntry
    //ověřte správné volání konstruktoru při vytvoření ItemPurchaseArchiveEntry

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private static final Item[] storageItems =
    {
        new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
        new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
        new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
        new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
        new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
        new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
    };

    private HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive;

    public PurchasesArchiveTest()
    {
        // Creating HashMap
        itemPurchaseArchive = new  HashMap<Integer, ItemPurchaseArchiveEntry>();
        // Filling hashmap with id as a key, and ItemPurchaseArchiveEntry witch uses static array of items for initializing
        for (int i = 0; i < storageItems.length; i++)
        {
            itemPurchaseArchive.put(i, new ItemPurchaseArchiveEntry(storageItems[i]));
        }
    }

    @BeforeEach
    public void SetUpStreams()
    {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    @AfterEach
    public void RestoreStreams()
    {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void PrintItemPurchaseStatisticsTest()
    {
        //region ARRANGE
        //Initializing expected string value with static array of items and default sold value to 1
        String expectedOutput = "ITEM PURCHASE STATISTICS:"+System.lineSeparator();
        for (int i = 0; i < storageItems.length; i++)
        {
            expectedOutput +="ITEM  " + storageItems[i].toString()+"   HAS BEEN SOLD "+1+" TIMES"+System.lineSeparator();
        }

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchive, null);
        //endregion

        //region ACT
        purchasesArchive.printItemPurchaseStatistics();
        //endregion

        //region ASSERT
        assertEquals(expectedOutput, outContent.toString());
        //endregion
    }

    @ParameterizedTest()
    @MethodSource("ProvideArguments")
    public void getHowManyTimesHasBeenItemSoldTest(Item item)
    {
        //region ARRANGE
        int expected = 1;
        //endregion

        //region ACT
        int result = 0;
        if (!itemPurchaseArchive.containsKey(item.getID())) { expected = 0; }
        else
        {
            result = itemPurchaseArchive.get(item.getID()).getCountHowManyTimesHasBeenSold();
        }
        //endregion

        //region ASSERT
        Assertions.assertEquals(expected, result);
        //endregion
    }

    //Not implemented
//    @Test
//    public void putOrderToPurchasesArchive()
//    {
//        //region ARRANGE
//        Order orderMocked = Mockito.mock(Order.class);
//        Mockito.when(orderMocked.getItems()).thenReturn(GetItemsForOrder());
//
//        var hm = new HashMap<Integer, ItemPurchaseArchiveEntry>();
//        hm.put(0, new ItemPurchaseArchiveEntry(storageItems[0]));
//
//        var expectedVal = hm.get(0).getCountHowManyTimesHasBeenSold() +1;
//
//        //endregion
//
//        //region ACT
//
//        PurchasesArchive purchasesArchive = new PurchasesArchive(hm, new ArrayList());
//
//        purchasesArchive.putOrderToPurchasesArchive(orderMocked);
//        //endregion
//
//        //region ASSERT
//        //endregion
//    }

    private ArrayList<Item> GetItemsForOrder()
    {
        return (ArrayList<Item>)Arrays.asList(storageItems);
    }

    private static Stream<Arguments> ProvideArguments()
    {
        return Stream.of
        (
            Arguments.of
            (
                storageItems[0],
                storageItems[1],
                storageItems[2],
                new StandardItem(-1, "negativeId", 5000, "GADGETS", 5)
            )
        );
    }
}

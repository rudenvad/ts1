package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import shop.Item;
import shop.StandardItem;

import java.util.stream.Stream;

/**
 * The test class for testing ItemStock.
 * Tested methods:
 * (1) ItemStick constructor with 1 parameter
 * (2) IncreaseItemCount with 1 parameter
 * (3) decreaseItemCount with 1 parameter
 */

//konstruktor, parametrizovaný test pro metody změny počtu
public class ItemStockTest
{
    /**
     * Parametrized method, tests ItemStock constructor for proper work.
     */
    @ParameterizedTest(name ="item:{0}")
    @MethodSource("ProvideArguments")
    public void ItemStockConstructorTest(Item refItem)
    {
        //region ARRANGE

        // Expected data
        Item expectedItem = refItem;
        int expectedCount = 0;

        ItemStock itemStock = new ItemStock(refItem);
        //endregion

        //region ACT

        //Result data
        Item resultItem = itemStock.getItem();
        int resultCount = itemStock.getCount();
        //endregion

        //region ASSERT
        Assertions.assertEquals(expectedCount, resultCount, "\nCkeck count failed!");
        Assertions.assertEquals(expectedItem, resultItem, "\nCkeck items failed!");
        //endregion
    }

    /**
     * Parametrized method, tests ItemStock increasing count method for proper work.
     */
    @ParameterizedTest(name ="x:{0}")
    @MethodSource("ProvideIntArguments")
    public void IncreaseItemCountTest(int x)
    {
        //region ARRANGE
        ItemStock itemStock = new ItemStock(new StandardItem(0,"Dancing Panda v.2", 5000, "GADGETS", 5));
        int expectedCountValue = itemStock.getCount() + x;
        int expectedSecondCountValue = expectedCountValue + x;
        //endregion

        //region ACT
        itemStock.IncreaseItemCount(x);
        int resultCountValue = itemStock.getCount();
        itemStock.IncreaseItemCount(x);
        int resultSecondCountValue = itemStock.getCount();
        //endregion

        //region ASSERT
        Assertions.assertEquals(expectedCountValue,resultCountValue, "\nCheck count failed!");
        Assertions.assertEquals(expectedSecondCountValue,resultSecondCountValue, "\nCheck count failed!");
        //endregion

    }

    /**
     * Parametrized method, tests ItemStock decreasing count method for proper work.
     */
    @ParameterizedTest(name ="x:{0}")
    @MethodSource("ProvideIntArguments")
    public void DecreaseItemCountTest(int x)
    {
        //region ARRANGE
        ItemStock itemStock = new ItemStock(new StandardItem(0,"Dancing Panda v.2", 5000, "GADGETS", 5));
        int expectedCountValue = itemStock.getCount() - x;
        int expectedSecondCountValue = expectedCountValue - x;
        //endregion

        //region ACT
        itemStock.decreaseItemCount(x);
        int resultCountValue = itemStock.getCount();
        itemStock.decreaseItemCount(x);
        int resultSecondCountValue = itemStock.getCount();
        //endregion

        //region ASSERT
        Assertions.assertEquals(expectedCountValue,resultCountValue, "\nCheck count failed!");
        Assertions.assertEquals(expectedSecondCountValue,resultSecondCountValue, "\nCheck count failed!");
        //endregion

    }

    /**
     * Method that provides arguments for ItemStockConstructorTest method
     */
    private static Stream<Arguments> ProvideArguments()
    {
        return Stream.of
            (
                Arguments.of(new StandardItem(1,"Dancing Panda v.2", 5000, "GADGETS", 5)),
                Arguments.of(new StandardItem(2,"Dancing Panda v.3 with USB port", 6000, "GADGETS", 10)),
                Arguments.of(new StandardItem(3,"Screwdriver", 200, "TOOLS", 5)),
                Arguments.of(new StandardItem(4,"Star Wars Jedi buzzer", 500, "GADGETS", 30)),
                Arguments.of(new StandardItem(5,"Angry bird cup", 300, "GADGETS", 20)),
                Arguments.of(new StandardItem(6,"Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10))
            );
    }

    /**
     * Method that provides arguments for IncreaseItemCountTest and DecreaseItemCountTest methods
     */
    private static Stream<Arguments> ProvideIntArguments()
    {
        return Stream.of
            (
                Arguments.of(-10), Arguments.of(-9), Arguments.of(-8),
                Arguments.of(-7), Arguments.of(-6), Arguments.of(-5),
                Arguments.of(-4), Arguments.of(-3), Arguments.of(-2),
                Arguments.of(-1), Arguments.of(0), Arguments.of(1),
                Arguments.of(2), Arguments.of(3), Arguments.of(4),
                Arguments.of(5), Arguments.of(6), Arguments.of(7),
                Arguments.of(8), Arguments.of(9)
            );
    }
}

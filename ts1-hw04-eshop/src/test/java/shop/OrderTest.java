package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * The test class for testing Order.
 * Tested methods:
 * (1) Order constructor with 4 parameters
 * (2) Order constructor with 3 parameters
 */
public class OrderTest
{
    /**
     * Parametrized method, tests Order constructor with 4 arguments for proper work.
     */
    @ParameterizedTest(name ="shoppingCart:{0}, customerName:{1}, customerAddress:{2}, state:{3}")
    @MethodSource("ProvideArguments")
    public void OrderConstructorWithFourParametersTest(ShoppingCart cart, String customerName, String customerAddress, int state)
    {
        if(cart == null || customerName == null || customerAddress == null)
        {
            Assertions.fail("\nArguments cannot be null");
        }

        //region ARRANGE

        // Expected data
        ArrayList<Item> expectedItems = cart.getCartItems();
        String expectedName = customerName;
        String expectedCustomerAddress = customerAddress;
        int expectedState = state;

        Order order = new Order(cart, customerName, customerAddress, state);
        //endregion

        //region ACT

        //Result data
        ArrayList<Item> resultItems = order.getItems();
        String resultName = order.customerName;
        String resultCustomerAddress = order.customerAddress;
        int resultState = order.state;

        //endregion

        //region ASSERT
        Assertions.assertEquals(expectedItems, resultItems, "\nCkeck items failed!");
        Assertions.assertEquals(expectedName, resultName,"\nCkeck name failed!");
        Assertions.assertEquals(expectedCustomerAddress, resultCustomerAddress, "\nCkeck customer address failed!");
        Assertions.assertEquals(expectedState, resultState, "\nCkeck state failed!");
        //endregion
    }

    /**
     * Parametrized method, tests Order constructor with 3 arguments for proper work.
     */
    @ParameterizedTest(name ="shoppingCart:{0}, customerName:{1}, customerAddress:{2}")
    @MethodSource("ProvideArguments")
    public void OrderConstructorWithThreeParametersTest(ShoppingCart cart, String customerName, String customerAddress)
    {
        if(cart == null || customerName == null || customerAddress == null)
        {
            Assertions.fail("\nArguments cannot be null");
        }

        //region ARRANGE

        // Expected data
        ArrayList<Item> expectedItems = cart.getCartItems();
        String expectedName = customerName;
        String expectedCustomerAddress = customerAddress;
        int expectedState = 0;

        Order order = new Order(cart, customerName, customerAddress);
        //endregion

        //region ACT

        //Result data
        ArrayList<Item> resultItems = order.getItems();
        String resultName = order.customerName;
        String resultCustomerAddress = order.customerAddress;
        int resultState = order.state;
        //endregion

        //region ASSERT
        Assertions.assertEquals(expectedItems, resultItems, "\nCkeck items failed!");
        Assertions.assertEquals(expectedName, resultName,"\nCkeck name failed!");
        Assertions.assertEquals(expectedCustomerAddress, resultCustomerAddress, "\nCkeck customer address failed!");
        Assertions.assertEquals(expectedState, resultState, "\nCkeck state failed!");
        //endregion
    }

    /**
     * Array of items for shopping cart filling
     */
    private static final Item[] storageItems =
    {
        new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
        new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
        new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
        new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
        new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
        new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
    };

    /**
     * Method that generates list of items for shopping cart,
     * @return ArrayList<Item>
     */
    private static ArrayList<Item> PrepareItemsListToShoppingCart(int count)
    {
        ArrayList<Item> items = new ArrayList<Item>();
        int tmp = 0;
        for(int i = 0; i < count; i++)
        {
            items.add(storageItems[tmp]);
            tmp++;
            if (tmp >= storageItems.length) { tmp = 0; }
        }
        return items;
    }

    /**
     * Method that provides arguments for OrderConstructorWithFourParametersTest(...) and OrderConstructorWithThreeParametersTest(...) methods
     */
    private static Stream<Arguments> ProvideArguments()
    {
        return Stream.of
        (
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(1)),"Irena Mokry","Delnicka 95", 1),
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(2)),"Klara Orsag","Jablonova 1532", 2),
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(3)),"Renata Zlamal","Jana Stastneho 1152", 3),
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(4)),"Kristyna Stehlik","Svepomoc 1055", 4),
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(5)),"Eva Pavelka","Skolni 1315", 5),
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(6)),"Nikola Konecna","Nove Mesto 523", 6),
            Arguments.of(null,"Zuzana Kaderabek","Delnicka 22", 7),
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(7)), null,"Delnicka 22", 7),
            Arguments.of(new ShoppingCart(PrepareItemsListToShoppingCart(8)),"Nikola Orsag", null, 8)
        );
    }
}

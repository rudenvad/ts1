package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * The test class for testing StandardItem.
 * Tested methods:
 * (1) Constructor
 * (2) Copy
 * (3) Equals
 */
public class StandardItemTest
{
    /**
     * Parametrized method, tests StandardItem constructor for proper work.
     */
    @ParameterizedTest(name ="id:{0}, name:{1}, price:{2}, category:{3}, loyaltyPoints:{4}")
    @MethodSource("ProvideArguments")
    public void StandardItemConstructorTest(int id, String name, float price, String category, int loyaltyPoints)
    {
        //region ARRANGE

        // Expected data
        int expectedID = id;
        String expectedName = name;
        float expectedPrice = price;
        String expectedCategory = category;
        int expectedLoyaltyPoints = loyaltyPoints;

        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        //endregion

        //region ACT

        //Result data
        int resultID = standardItem.getID();
        String resultName = standardItem.getName();
        float resultPrice = standardItem.getPrice();
        String resultCategory = standardItem.getCategory();
        int resultLoyaltyPoints = standardItem.getLoyaltyPoints();
        //endregion

        //region ASSERT
        Assertions.assertEquals(expectedID, resultID, "\nCkeck id failed!");
        Assertions.assertEquals(expectedName, resultName,"\nCkeck name failed!");
        Assertions.assertEquals(expectedPrice, resultPrice, "\nCkeck price failed!");
        Assertions.assertEquals(expectedCategory, resultCategory, "\nCkeck category failed!");
        Assertions.assertEquals(expectedLoyaltyPoints, resultLoyaltyPoints, "\nCkeck loyaltyPoints failed!");
        //endregion
    }

    /**
     * Parametrized method, tests Copy method for proper work.
     */
    @ParameterizedTest(name ="id:{0}, name:{1}, price:{2}, category:{3}, loyaltyPoints:{4}")
    @MethodSource("ProvideArguments")
    public void CopyTest(int id, String name, float price, String category, int loyaltyPoints)
    {
        //region ARRANGE
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);

        //Expected data
        int expectedID = standardItem.getID();
        String expectedName = standardItem.getName();
        float expectedPrice = standardItem.getPrice();
        String expectedCategory = standardItem.getCategory();
        int expectedLoyaltyPoints = standardItem.getLoyaltyPoints();
        //endregion

        //region ACT
        StandardItem copyStandardItem = standardItem.copy();

        //Result data
        int resultID = copyStandardItem.getID();
        String resultName = copyStandardItem.getName();
        float resultPrice = copyStandardItem.getPrice();
        String resultCategory = copyStandardItem.getCategory();
        int resultLoyaltyPoints = copyStandardItem.getLoyaltyPoints();
        //endregion

        //region ASSERT
        Assertions.assertEquals(expectedID, resultID, "\nCkeck id failed!");
        Assertions.assertEquals(expectedName, resultName,"\nCkeck name failed!");
        Assertions.assertEquals(expectedPrice, resultPrice, "\nCkeck price failed!");
        Assertions.assertEquals(expectedCategory, resultCategory, "\nCkeck category failed!");
        Assertions.assertEquals(expectedLoyaltyPoints, resultLoyaltyPoints, "\nCkeck loyaltyPoints failed!");
        //endregion
    }

    /**
     * Parametrized method, tests Equals method for proper work.
     */
    @ParameterizedTest(name ="item:{0}")
    @MethodSource("ProvideArgumentsForEqualsTest")
    public void EqualsTest(StandardItem item, Object object)
    {
        //region ARRANGE
        StandardItem firstStandardItem = item;
        Object secondItemToEqualWith = object;

        //expecting value default is true, if items are equal it would be still true
        boolean expected = true;

        boolean result = true;

        //endregion

        //region ACT

        //if secondItemToEqualWith is not instance of StandartItem --> method should return false value, so expecting value should be changed to false
        if (!(secondItemToEqualWith instanceof StandardItem)) { expected = false; }
        else
        {
            StandardItem second = (StandardItem)secondItemToEqualWith;
            //if 2 items are not equal --> method should return false,so expecting value should be changed to false
            if
            (!(
                    firstStandardItem.getID() == second.getID() &&
                    firstStandardItem.getName().equals(second.getName()) &&
                    firstStandardItem.getPrice() == second.getPrice() &&
                    firstStandardItem.getCategory().equals(second.getCategory())
            ))
            {
                expected = false;
            }
        }

        result = firstStandardItem.equals(secondItemToEqualWith);
        //endregion

        //region ASSERT
        Assertions.assertEquals(expected, result, "\nReturn value is not correct");
        //endregion
    }

    /**
     * Method that provides arguments for StandardItemConstructorTest(...) and CopyTest(...) methods
     */
    private static Stream<Arguments> ProvideArguments()
    {
        return Stream.of
            (
                    Arguments.of(1,"Dancing Panda v.2", 5000, "GADGETS", 5),
                    Arguments.of(2,"Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                    Arguments.of(3,"Screwdriver", 200, "TOOLS", 5),
                    Arguments.of(4,"Star Wars Jedi buzzer", 500, "GADGETS", 30),
                    Arguments.of(5,"Angry bird cup", 300, "GADGETS", 20),
                    Arguments.of(6,"Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10)
            );
    }

    /**
     * Method that provides arguments for EqualsTest method
     */
    private static Stream<Arguments> ProvideArgumentsForEqualsTest()
    {
        return Stream.of
            (
                Arguments.of
                    (
                        new StandardItem(1,"Dancing Panda v.2", 5000, "GADGETS", 5),
                        new StandardItem(1,"Dancing Panda v.2", 5000, "GADGETS", 5)
                    ),
                Arguments.of
                    (
                        new StandardItem(2,"Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                        new StandardItem(2,"Dancing Panda v.3 with USB port", 6000, "GADGETS", 10)
                    ),
                Arguments.of
                    (
                        new StandardItem(3,"Screwdriver", 200, "TOOLS", 5),
                        new StandardItem(3,"Screwdriver", 200, "TOOLS", 5)
                    ),
                Arguments.of
                    (
                        new StandardItem(4,"Star Wars Jedi buzzer", 500, "GADGETS", 30),
                        new StandardItem(4,"Star Wars Jedi buzzer", 500, "GADGETS", 30)
                    ),
                Arguments.of
                    (
                        new StandardItem(5,"Angry bird cup", 300, "GADGETS", 20),
                        new StandardItem(5,"Angry bird cup", 300, "GADGETS", 20)
                    ),
                Arguments.of
                    (   /*Check with different values (2 parameter is not the same)*/
                        new StandardItem(6,"Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10),
                        new StandardItem(6,"Not Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10)
                    ),
                Arguments.of
                    (
                        /*Check with comparing Item to null */
                        new StandardItem(7,"PhoneRV", 10000, "GADGETS", 9),
                        null
                    ),
                Arguments.of
                    (
                        /*Check with comparing Item to String */
                        new StandardItem(8,"PhoneRV Pro", 122000, "GADGETS", 3),
                        "some string"
                    ),
                Arguments.of
                    (
                        /*Check with comparing Item to Integer */
                        new StandardItem(9,"LaptopRV Pro", 222000, "GADGETS", 5),
                        95
                    )
            );
    }
}
package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

// MethodName_StateUnderTest_ExpectedBehavior

public class RudenvadTest
{
    @Test
    public void factorial_nIs5_returns120()
    {
        //Arrange
        Rudenvad rudenvad = new Rudenvad();
        int n = 5;
        int expected = 120;

        //Act
        long actual = rudenvad.factorial(n);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorial_nIsMinus1_returns0()
    {
        //Arrange
        Rudenvad rudenvad = new Rudenvad();
        int n = -1;
        int expected = 0;

        //Act
        long actual = rudenvad.factorial(n);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorial_nIs0_returns1()
    {
        //Arrange
        Rudenvad rudenvad = new Rudenvad();
        int n = 0;
        int expected = 1;

        //Act
        long actual = rudenvad.factorial(n);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
}

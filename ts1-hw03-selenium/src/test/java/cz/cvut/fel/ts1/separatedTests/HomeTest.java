package cz.cvut.fel.ts1.separatedTests;

import cz.cvut.fel.ts1.WebDriverFactory;
import cz.cvut.fel.ts1.pages.HomePage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.stream.Stream;

/**
 *  Task 1:
 *  Home page URL: https://link.springer.com/
 *  (1) Move to Login page
 *  (2) Move to Advanced search page
 * */
public class HomeTest
{
    private static final String linkFromHomeToLogin = "https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F";
    private static final String linkFromHomeToAdvancedSearch = "https://link.springer.com/advanced-search";

    private WebDriverFactory webDriverFactory = new WebDriverFactory();
    private WebDriver webDriver;

    @BeforeEach
    public void Init()
    {
        webDriver = webDriverFactory.CreateWebDriver(HomePage.getHomeUrl());
        new WebDriverWait(webDriver, 5).until(ExpectedConditions.elementToBeClickable(webDriver.findElement(new By.ById("onetrust-accept-btn-handler")))).click();
    }

    /**
     *  (1) Move to Login page test
     */
    @Test
    public void MoveToLoginPageTest()
    {
        //ARRANGE
        HomePage homePage = new HomePage(webDriver);

        //ACT
        homePage.GoToLogin();

        //ASSERT
        Assertions.assertEquals(linkFromHomeToLogin,webDriver.getCurrentUrl());
    }

    /**
     *  (2) Move to Advanced search page
     */
    @Test
    public void MoveToAdvancedSearchPageTest()
    {
        //ARRANGE
        HomePage homePage = new HomePage(webDriver);

        //ACT
        homePage.GoToAdvancedSearchPage();

        //ASSERT
        Assertions.assertEquals(linkFromHomeToAdvancedSearch,webDriver.getCurrentUrl());
    }
}

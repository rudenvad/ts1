package cz.cvut.fel.ts1;

import cz.cvut.fel.ts1.pages.AdvancedSearchPage;
import cz.cvut.fel.ts1.pages.HomePage;
import cz.cvut.fel.ts1.pages.LoginPage;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.provider.Arguments;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FullTest
{
    private static final String linkFromHomeToLogin = "https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F";
    private static final String linkFromHomeToAdvancedSearch = "https://link.springer.com/advanced-search";
    private static final String linkFromAdvancedSearchResult = "https://link.springer.com/search?date-facet-mode=between&facet-start-year=2021&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29&showAll=true&facet-end-year=2021#";

    private static final String userName = "Taras Shevchenko";

    private WebDriverFactory webDriverFactory = new WebDriverFactory();
    private WebDriver webDriver;

    private List<Article> articles = new ArrayList<>();

    @BeforeEach
    public void Init()
    {
        webDriver = webDriverFactory.CreateWebDriver(HomePage.getHomeUrl());
        var wait = new WebDriverWait(webDriver, 10);
        try
        {
            wait.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(new By.ById("onetrust-accept-btn-handler")))).click();
        }
        catch (NoSuchElementException ex)
        {
            System.out.println(ex.getMessage());
        }

    }

//    @AfterEach public void Close() { webDriver.close(); }

    @Test
    public void Test()
    {
        MoveToAdvancedSearchPageTest();

        FillDataAndSearchTest();

        ChoseContetType();

        GetTitleDateDOI();

        webDriver.navigate().to(HomePage.getHomeUrl());

        MoveToLoginPageFromHomePageTest();

        FillDataAndLoginTest();

        for (var article : articles) { CheckArticle(article); }
    }

    /**
     *  (1) Move to Advanced search page
     */
    @Test
    public void MoveToAdvancedSearchPageTest()
    {
        //ARRANGE
        HomePage homePage = new HomePage(webDriver);

        //ACT
        homePage.GoToAdvancedSearchPage();

        //ASSERT
        Assertions.assertEquals(linkFromHomeToAdvancedSearch,webDriver.getCurrentUrl());
    }

    /**
     *  (2) Fill data and search:
     *  checks changing url from advanced search page to search page
     */
    @Test
    public void FillDataAndSearchTest()
    {
        //ARRANGE
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(webDriver);

        //ACT
        advancedSearchPage.FillAllWords();
        advancedSearchPage.FillAtLeastOneOfTheWords();
        advancedSearchPage.FillStartYear();
        advancedSearchPage.FillEndYear();

        advancedSearchPage.Submit();

        //ASSERT
        Assertions.assertEquals(linkFromAdvancedSearchResult,webDriver.getCurrentUrl());
    }


    /**
     *  (3)
     */
    @Test
    public void ChoseContetType()
    {
        //ARRANGE
        WebElement articleLink = webDriver.findElement(new By.ByCssSelector("#content-type-facet > ol > li:nth-child(2) > a"));

        articleLink.click();

        var result =webDriver.findElement(new By.ByCssSelector("#kb-nav--main > div.header > p > a:nth-child(1)"));
        System.out.println(result.getText());

        //ASSERT
        Assertions.assertEquals("Article",result.getText());
    }

    /**
     *  (4)
     */
    @Test
    public void GetTitleDateDOI()
    {
        for(int i = 0; i < 4; i++)
        {
            WebElement resultList = webDriver.findElement(new By.ById("results-list"));
            List<WebElement> resultArticles = resultList.findElements(new By.ByTagName("li"));

            var link = resultArticles.get(i).findElement(new By.ByTagName("a"));
            link.click();

            var title = webDriver.findElement(new By.ByCssSelector("#main-content > main > article > div.c-article-header > header > h1")).getText();
            var date = webDriver.findElement(new By.ByTagName("time")).getText();
            var doi = webDriver.findElement(new By.ByCssSelector("#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--doi > p > span.c-bibliographic-information__value > a")).getText();

            articles.add(new Article(title, date, doi));
            webDriver.navigate().back();
        }

        int count = 0;
        for(var article : articles)
        {
            System.out.println(count++);
            System.out.println("Title:"+ article.getTitle());
            System.out.println("Date:"+ article.getDate());
            System.out.println("DOI:"+ article.getDoi());
        }
    }

    /**
     *  (5) Move to Login page test
     */
    @Test
    public void MoveToLoginPageFromHomePageTest()
    {
        //ARRANGE
        HomePage homePage = new HomePage(webDriver);

        //ACT
        homePage.GoToLogin();

        //ASSERT
        Assertions.assertEquals(linkFromHomeToLogin,webDriver.getCurrentUrl());
    }

    /**
     *  (6) Login with credentials:
     *  email: tarasshevchenkonekripak@gmail.com
     *  password: Nekrip@k1814
     *
     *  checks changing url from login page to home page
     *  checks if button with id 'user' is not null (it means user was logged in)
     *  checks if span of button is not null
     *  checks if username as is expected
     */
    @Test
    public void FillDataAndLoginTest()
    {
        //ARRANGE
        LoginPage loginPage = new LoginPage(webDriver);

        //ACT

        loginPage.FillEmail();
        loginPage.FillPassword();

        loginPage.Submit();

        //ASSERT
        Assertions.assertEquals(HomePage.getHomeUrl(),webDriver.getCurrentUrl());

        WebElement userButton = webDriver.findElement(new By.ById("user"));
        Assertions.assertNotNull(userButton);

        WebElement spanName = webDriver.findElement(new By.ByCssSelector("#user > span"));
        Assertions.assertNotNull(spanName);
        Assertions.assertEquals(userName, spanName.getText());
    }


    @Test
    public void SearchByArticleTitle(String title)
    {
        var searchInput = webDriver.findElement(new By.ById("query"));
        var searchButton = webDriver.findElement(new By.ById("search"));
        searchInput.clear();
        searchInput.sendKeys(title);
        searchButton.click();
    }

    @Test
    public void CheckArticle(Article article)
    {
        SearchByArticleTitle(article.getTitle());

        var link = webDriver.findElement(new By.ByCssSelector("#results-list > li:nth-child(1) > h2 > a"));
        link.click();

        var date = webDriver.findElement(new By.ByTagName("time")).getText();
        var doi = webDriver.findElement(new By.ByCssSelector("#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--doi > p > span.c-bibliographic-information__value > a")).getText();

        Assertions.assertEquals(article.getDate(), date);
        Assertions.assertEquals(article.getDoi(), doi);

        webDriver.navigate().back();
    }
}

package cz.cvut.fel.ts1.separatedTests;

import cz.cvut.fel.ts1.WebDriverFactory;
import cz.cvut.fel.ts1.pages.AdvancedSearchPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

public class AdvancedSearchTest
{
    private static final String searchResult = "https://link.springer.com/search?date-facet-mode=between&facet-start-year=2021&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29&showAll=true&facet-end-year=2021#";

    private WebDriverFactory webDriverFactory = new WebDriverFactory();
    private WebDriver webDriver;

    @BeforeEach
    public void CreateWebDriver() { webDriver = webDriverFactory.CreateWebDriver(AdvancedSearchPage.getAdvancedSearchUrl()); }

    /**
     *  (1) Fill data and search:
     *  checks changing url from advanced search page to search page
     */
    @Test
    public void FillDataAndSearchTest()
    {
        //ARRANGE
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(webDriver);

        //ACT
        advancedSearchPage.FillAllWords();
        advancedSearchPage.FillAtLeastOneOfTheWords();
        advancedSearchPage.FillStartYear();
        advancedSearchPage.FillEndYear();

        advancedSearchPage.Submit();

        //ASSERT
        Assertions.assertEquals(searchResult,webDriver.getCurrentUrl());
    }
}

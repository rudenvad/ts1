package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends PageFactory
{
    private static final String HomeUrl = "https://link.springer.com/";

    private WebDriver driver;

    @FindBy(css = "#header > div.cross-nav.cross-nav--wide > div.auth.flyout > a")
    private WebElement loginLink;

    @FindBy(css = "#search-options > button")
    private WebElement searchButton;

    @FindBy(id = "advanced-search-link")
    private WebElement advancedSearchLink;

    @FindBy(id = "onetrust-accept-btn-handler")
    WebElement trustPolicyButton;

    public HomePage(WebDriver driver)
    {
        initElements(driver, this);
        this.driver = driver;
    }

    //These method is added because trust policy always appears on screen
    public void AcceptAllCookies() { trustPolicyButton.click(); }

    public void GoHome() { driver.navigate().to(HomeUrl); }

    public void GoToLogin() { loginLink.click(); }

    public void GoToAdvancedSearchPage()
    {
        searchButton.click();
        advancedSearchLink.click();
    }

    //region Getters
    public WebElement getLoginLink() { return loginLink; }
    public WebElement getSearchButton() { return searchButton; }
    public WebElement getAdvancedSearchLink() { return advancedSearchLink; }
    public static String getHomeUrl() { return HomeUrl; }
    //endregion
}

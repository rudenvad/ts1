package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdvancedSearchPage extends PageFactory
{
    private static final String advancedSearchUrl = "https://link.springer.com/advanced-search ";
    private static final String allWords = "Page Object Model";
    private static final String atLeastWords = "Sellenium Testing";
    private static  final String year = "2021";

    private WebDriver driver;

    @FindBy(id = "all-words")
    private WebElement allWordsInput;

//    @FindBy(id = "exact-phrase")
//    private WebElement exactPhraseInput;

    @FindBy(id = "least-words")
    private WebElement atLeastWordsInput;

//    @FindBy(id = "title-is")
//    private WebElement titleInput;
//
//    @FindBy(id = "author-is")
//    private WebElement authorInput;

//    @FindBy(id = "date-facet-mode")
//    private WebElement dateFacetMode;

    @FindBy(id = "facet-start-year")
    private WebElement facetStartYearInput;

    @FindBy(id = "facet-end-year")
    private WebElement facetEndYearInput;

    @FindBy(id = "submit-advanced-search")
    private WebElement submitAdvancedSearch;

    @FindBy(id = "onetrust-accept-btn-handler")
    WebElement trustPolicyButton;

    public AdvancedSearchPage(WebDriver driver)
    {
        initElements(driver, this);
        this.driver = driver;
    }

    public void AcceptTrustPolicy() { trustPolicyButton.click(); }
    public void FillAllWords() { allWordsInput.sendKeys(allWords); }
    public void FillAtLeastOneOfTheWords() { atLeastWordsInput.sendKeys(atLeastWords); }
    public void FillStartYear() { facetStartYearInput.sendKeys(year); }
    public void FillEndYear() { facetEndYearInput.sendKeys(year); }

    public void Submit()
    {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(submitAdvancedSearch)).click();
//        submitAdvancedSearch.click();
    }

    public static String getAdvancedSearchUrl() { return advancedSearchUrl; }
    public static String getAllWords() { return allWords; }
    public static String getAtLeastWords() { return atLeastWords; }
    public static String getYear() { return year; }

    public WebElement getAllWordsInput() { return allWordsInput; }
    public WebElement getAtLeastWordsInput() { return atLeastWordsInput; }
    public WebElement getFacetStartYearInput() { return facetStartYearInput; }
    public WebElement getFacetEndYearInput() { return facetEndYearInput; }
    public WebElement getSubmitAdvancedSearch() { return submitAdvancedSearch; }
    public WebElement getTrustPolicyButton() { return trustPolicyButton; }
}

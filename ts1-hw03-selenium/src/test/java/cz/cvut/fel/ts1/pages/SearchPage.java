package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.support.PageFactory;

public class SearchPage extends PageFactory
{
    private static final String searchPageUrl = "https://link.springer.com/search";
}

package cz.cvut.fel.ts1.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends PageFactory
{
    private static final String loginUrl = "https://link.springer.com/signup-login";
    private static final String email = "tarasshevchenkonekripak@gmail.com";
    private static final String password = "Nekrip@k1814";

    private WebDriver driver;

    @FindBy(id = "login-box-email")
    WebElement emailInput;

    @FindBy(id = "login-box-pw")
    WebElement passwordInput;

    @FindBy(css = "#login-box > div > div.form-submit > button")
    WebElement loginButton;

    @FindBy(id = "onetrust-accept-btn-handler")
    WebElement trustPolicyButton;

    public LoginPage(WebDriver driver)
    {
        initElements(driver, this);
        this.driver = driver;
    }

    //These method is added because trust policy always appears on screen
    public void AcceptAllCookies() { trustPolicyButton.click(); }

    public void FillEmail() { this.emailInput.sendKeys(email); }
    public void FillPassword() { this.passwordInput.sendKeys(password); }
    public void Submit() { loginButton.click(); }

    public static String getLoginUrl() { return loginUrl; }
    public static String getEmail() { return email; }
    public static String getPassword() { return password; }

    public WebElement getEmailInput() { return emailInput; }
    public WebElement getPasswordInput() { return passwordInput; }
    public WebElement getLoginButton() { return loginButton; }
}

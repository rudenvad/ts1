package cz.cvut.fel.ts1.separatedTests;

import cz.cvut.fel.ts1.WebDriverFactory;
import cz.cvut.fel.ts1.pages.HomePage;
import cz.cvut.fel.ts1.pages.LoginPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *  Task 2:
 *  Login page URL: https://link.springer.com/signup-login
 *  (1) Login
 * */
public class LoginTest
{
    private static final String userName = "Taras Shevchenko";

    private WebDriverFactory webDriverFactory = new WebDriverFactory();
    private WebDriver webDriver;

    @BeforeEach
    public void CreateWebDriver() { webDriver = webDriverFactory.CreateWebDriver(LoginPage.getLoginUrl()); }

    /**
     *  (1) Login with credentials:
     *  email: tarasshevchenkonekripak@gmail.com
     *  password: Nekrip@k1814
     *
     *  checks changing url from login page to home page
     *  checks if button with id 'user' is not null (it means user was logged in)
     *  checks if span of button is not null
     *  checks if username as is expected
     */
    @Test
    public void FillDataAndLoginTest()
    {
        //ARRANGE
        LoginPage loginPage = new LoginPage(webDriver);

        //ACT

        loginPage.FillEmail();
        loginPage.FillPassword();

        loginPage.Submit();

        //ASSERT
        Assertions.assertEquals(HomePage.getHomeUrl(),webDriver.getCurrentUrl());

        WebElement userButton = webDriver.findElement(new By.ById("user"));
        Assertions.assertNotNull(userButton);

        WebElement spanName = webDriver.findElement(new By.ByCssSelector("#user > span"));
        Assertions.assertNotNull(spanName);
        Assertions.assertEquals(userName, spanName.getText());
    }
}

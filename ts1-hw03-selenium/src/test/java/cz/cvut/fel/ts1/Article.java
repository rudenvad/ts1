package cz.cvut.fel.ts1;

public class Article
{
    private String title;
    private String date;
    private String doi;

    public Article(String title, String date, String doi)
    {
        this.title = title;
        this.date = date;
        this.doi = doi;
    }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }
    public String getDoi() { return doi; }
    public void setDoi(String doi) { this.doi = doi; }
}


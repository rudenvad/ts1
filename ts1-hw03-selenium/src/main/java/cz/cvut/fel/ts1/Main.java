package cz.cvut.fel.ts1;

import org.openqa.selenium.WebDriver;

public class Main
{
    public static void main(String[] args)
    {
        WebDriverFactory wf = new WebDriverFactory();
        WebDriver webDriver = wf.CreateWebDriver("https://link.springer.com/");
    }
}

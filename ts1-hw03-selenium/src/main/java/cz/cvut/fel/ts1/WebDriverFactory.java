package cz.cvut.fel.ts1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;

public class WebDriverFactory
{
    public WebDriver CreateWebDriver(String url)
    {
        System.setProperty("webdriver.chrome.driver", new File(Main.class.getClassLoader().getResource("chromedriver.exe").getPath()).toString());
        WebDriver webDriver = new ChromeDriver();
        webDriver.get(url);

        return webDriver;
    }
}
